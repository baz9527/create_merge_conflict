---
title: "Übungsblatt 6"
author: "Team Foo"
date: "2022-11-10"
output:
  html_document:
    keep_md: true
    toc: true
    toc_float: true
    number_sections: true
  pdf_document:
    toc: yes
---


<!--
1.) Wenn Sie diese oder eine ähnliche Fehlermeldung sehen:
Fehler in library(sozoeko1): es gibt kein Paket namens 'sozoeko1' / 'AER'

Dann klicken Sie unten links auf Console und geben ab dem blinkenden Strich ein:

install.packages("remotes") 
library(remotes)
remotes::install_gitlab("baz9527/sozoeko1", host="gitlab.rrz.uni-hamburg.de")

(Die Installation des sozoeko1 Pakets dauert ein wenig, warten Sie in Ruhe ab bis zur Meldung: * DONE (sozoeko1))

2.) Wenn Sie diese oder eine ähnliche Fehlermeldung sehen:
Fehler in library(AER): es gibt kein Paket namens 'AER'

Dann klicken Sie unten links auf Console und geben ab dem blinkenden Strich ein:

install.packages("AER")

3.) Versuchen Sie dann, erneut das Rmd zu knitten.
Wenn alles funktioniert, können Sie diesen Kommentar (Zeile 19 bis einschließlich Zeile 31) 
löschen.
-->
# Aufgabe 1


